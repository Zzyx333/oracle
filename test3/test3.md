# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
-进行分区与不分区的对比实验。

### 创建sale用户

```sql
  SQL> show user
  USER 为 "SALE"
```

### 创建orders订单表
```sql
CREATE TABLE orders 
(
order_id NUMBER(9, 0) NOT NULL,
customer_name VARCHAR2(40 BYTE) NOT NULL,
customer_tel VARCHAR2(40 BYTE) NOT NULL,
order_date DATE NOT NULL,
employee_id NUMBER(6, 0) NOT NULL, 
discount NUMBER(8, 2) DEFAULT 0, 
trade_receivable NUMBER(8, 2) DEFAULT 0,
CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE SALETAB 
PCTFREE 10 INITRANS 1 
STORAGE (BUFFER_POOL DEFAULT) 
NOCOMPRESS NOPARALLEL 
PARTITION BY RANGE (order_date) 
( PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE SALETAB
 PCTFREE 10 
 INITRANS 1 
 STORAGE( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE SALETAB,
PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE SALETAB
);
表已创建。
```
### 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
( id NUMBER(9, 0) NOT NULL,
order_id NUMBER(10, 0) NOT NULL,
product_id VARCHAR2(40 BYTE) NOT NULL, 
product_num NUMBER(8, 2) NOT NULL, 
product_price NUMBER(8, 2) NOT NULL, 
CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  ),
CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE SALETAB
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
表已创建。
```
### 创建序列SEQ1的语句如下
```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
序列已创建。
```
### 插入100条orders记录的样例
```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i:= i+1;
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

- orders表的其他属性因为设置了非空，所以这里不适合做数据添加

SQL> insert into orders values(SEQ1.nextval,'zhangsan','1234567',to_date('2015-01-12','yyyy-MM-dd'),'123456',0,0);

已创建 1 行。

SQL> insert into orders values(SEQ1.nextval,'lisi','2345678',to_date('2015-02-12','yyyy-MM-dd'),'234567',1,1);

已创建 1 行。

已用时间:  00: 00: 00.01
SQL> insert into orders values(SEQ1.nextval,'wangwu','23456789',to_date('2015-03-12','yyyy-MM-dd'),'345678',2,2);

已创建 1 行。

已用时间:  00: 00: 00.01
SQL> insert into orders values(SEQ1.nextval,'wangwu','34567899',to_date('2015-04-12','yyyy-MM-dd'),'456789',2,2);

已创建 1 行。

已用时间:  00: 00: 00.00
SQL> insert into orders values(SEQ1.nextval,'zhaoliu','33567899',to_date('2015-05-12','yyyy-MM-dd'),'556789',4,4);

已创建 1 行。

- 查询
SQL> select * from orders;

  ORDER_ID CUSTOMER_NAME
---------- ----------------------------------------
CUSTOMER_TEL				 ORDER_DATE	     EMPLOYEE_ID
---------------------------------------- ------------------- -----------
  DISCOUNT TRADE_RECEIVABLE
---------- ----------------
	46 zhangsan
1234567 				 2015-01-12 00:00:00	  123456
	 0		  0

	47 lisi
2345678 				 2015-02-12 00:00:00	  234567
	 1		  1

  ORDER_ID CUSTOMER_NAME
---------- ----------------------------------------
CUSTOMER_TEL				 ORDER_DATE	     EMPLOYEE_ID
---------------------------------------- ------------------- -----------
  DISCOUNT TRADE_RECEIVABLE
---------- ----------------

	49 wangwu
23456789				 2015-03-12 00:00:00	  345678
	 2		  2

	50 wangwu
34567899				 2015-04-12 00:00:00	  456789

  ORDER_ID CUSTOMER_NAME
---------- ----------------------------------------
CUSTOMER_TEL				 ORDER_DATE	     EMPLOYEE_ID
---------------------------------------- ------------------- -----------
  DISCOUNT TRADE_RECEIVABLE
---------- ----------------
	 2		  2

	51 zhaoliu
33567899				 2015-05-12 00:00:00	  556789
	 4		  4


已用时间:  00: 00: 00.00
```

#### 比较
- 分区与不分区在数据量非常大的时候对比非常明显，当数据量庞大的时候，分区的优势就会特别明显，查询的速度就会非常的快
- 在数据量比较小的时候，二者的区别就不会很大，查询的速度不会有特别大的明显，都比较快。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

- 插入100条orders记录的样例脚本如下：

```sql
declare
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i:= i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

说明：
|| 表示字符连接符号
SEQ1是一个序列对象
```

## 实验注意事项

- 完成时间：2023-05-08，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test3目录中。
- 实验分析及结果文档说明书用Markdown格式编写。

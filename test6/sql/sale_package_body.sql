--------------------------------------------------------
--  文件已创建 - 星期四-五月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body SALE_PACKAGE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "O_MANAGER"."SALE_PACKAGE" AS

  PROCEDURE add_user(p_user_id in varchar2, p_user_account in char, p_user_age in number, p_user_sex in number, p_user_iden in char,
p_user_start in date, p_user_address in varchar2, p_user_pwd in char, p_user_tel in char ,p_user_register in number) AS
  BEGIN
    insert into t_user values(p_user_id, p_user_account, p_user_age, p_user_sex, p_user_iden , p_user_start, p_user_address, 
    p_user_pwd, p_user_tel, p_user_register);
  END add_user;
  
  PROCEDURE dele_user(user_id in varchar2,  user_iden in char) AS
  BEGIN
    delete from t_user t where user_id = t.user_id;
    delete from t_order o where user_id = o.user_id;
  END dele_user;
  
  function get_order_total( p_order_id in char) return number AS
    l_total number;
--    l_pro_price number := 0;
--    l_pro_num number :=0;
  BEGIN
  select order_pay into l_total from t_order where order_id = p_order_id;
  return l_total;
  END get_order_total;

END SALE_PACKAGE;

/

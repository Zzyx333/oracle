--------------------------------------------------------
--  文件已创建 - 星期四-五月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package USER_PACKAGE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "O_USER"."USER_PACKAGE" AS 
PROCEDURE dele_user(user_id in varchar2,  user_iden in char);
function get_order_total( p_order_id in char) return number;
END USER_PACKAGE;

/

--------------------------------------------------------
--  文件已创建 - 星期四-五月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package SALE_PACKAGE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "O_MANAGER"."SALE_PACKAGE" AS 
PROCEDURE add_user(p_user_id in varchar2, p_user_account in char, p_user_age in number, p_user_sex in number, p_user_iden in char,
p_user_start in date, p_user_address in varchar2, p_user_pwd in char, p_user_tel in char ,p_user_register in number);
PROCEDURE dele_user(user_id in varchar2,  user_iden in char);
function get_order_total( p_order_id in char) return number;
END SALE_PACKAGE;

/

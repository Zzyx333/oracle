--------------------------------------------------------
--  文件已创建 - 星期四-五月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body USER_PACKAGE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "O_USER"."USER_PACKAGE" AS
  PROCEDURE dele_user(user_id in varchar2,  user_iden in char) AS
  BEGIN
        delete from t_user t where user_id = t.user_id;
        delete from t_order o where user_id = o.user_id;
  END dele_user;

  function get_order_total( p_order_id in char) return number AS
    o_total number; 
  BEGIN
  select order_pay into o_total from t_order where order_id = p_order_id;
  return o_total;
  END get_order_total;
  
END USER_PACKAGE;

/

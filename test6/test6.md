<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|


## 基于Oracle数据库的商品销售系统的设计
### 1、创建用户
-- 创建o_user用户
```sql
SQL> create user o_user identified by 123;

用户已创建。
```
-- 创建o_manager用户
```sql
SQL> create user o_manager identified by 123;

用户已创建。
```

### 2、创建表空间
-- 创建用户类表空间
```sql
SQL> create tablespace user_table datafile'/tmp/user_table.dbf' size 50m;

表空间已创建。
```
-- 创建商品类表空间
```sql
SQL> create tablespace production_table datafile'/tmp/production_table。dbf' size 50m;

表空间已创建。
```
-- 创建订单类表空间
```sql
SQL> create tablespace order_table datafile'/tmp/order_table.dbf' size 50m;

表空间已创建。
```

### 3、将表空间赋给用户
-- 给o_user添加表空间
```sql
SQL> alter user o_user default tablespace order_table; 

SQL> alter user o_user quota unlimited on production_table;
用户已更改。
```

-- 给o_manager添加表空间
```sql
SQL> alter user o_manager default tablespace production_table;

SQL> alter user o_manager quota unlimited on user_table;

SQL> alter user o_manager quota unlimited on order_table;

用户已更改。
```

### 给用户授权
-- 给o_user授权
```sql
SQL> grant connect to o_user;

授权成功。

- 授权对应表的权限
SQL> grant delete on t_user to o_user;

授权成功。


SQL> grant delete on t_order to o_user;

授权成功。

SQL> grant select on t_production to o_user;

授权成功。
```
-- 给o_manager授权
```sql
SQL> grant connect to o_manager;
授权成功。

SQL> grant create table to o_manager;
授权成功。

SQL> grant insert any table to o_manager;
授权成功。

SQL> grant resource to o_manager;

授权成功。


```
### 创建表
```sql
-- 创建管理员表，放在user_table表空间中
SQL> create table t_manager(
  m_id char(10) constraint pk_t_m primary key,
  m_pwd char(8)
  ) tablespace user_table;

表已创建。

-- 创建用户表
SQL> create table t_user(
   user_id varchar2(25),
   user_account char(11),
   user_age number(3),
   user_sex number(1),
   user_iden char(11),
   user_start date,
   user_address varchar2(50),
   user_pwd char(18),
   user_tel char(11),
   user_register number(1));
表已创建。
SQL> alter table t_user add constraint pk_t_user primary key(user_id);
SQL> alter table t_user add constraint un_t_user unique(user_iden，user_account,user_tel);
SQL> alter table t_user modify user_register number(1) not null;
表已更改。

-- 修改表空间
SQL> alter table t_user move tablespace user_table;
表已更改。

--创建商品表
SQL> create table t_production(
  pro_id char(12) constraint pk_t_p primary key,
  pro_name varchar2(30) not null,
  pro_num number(4),
  pro_price number(10,2),
  pro_restock number(1)
  );

表已创建。
SQL> alter table t_production move tablespace production_table;

表已更改。

-- 创建订单管理表
SQL> create table t_order(
  order_id char(12) constraint pk_t_o primary key,
  user_id varchar2(25),
  order_total_price number(12,2),
  order_shipments number(1) not null,
  order_pay number(1) not null,
  constraint fk_t_o foreign key (user_id) references t_user(user_id));
表已创建。

SQL>alter table t_order move tablespace order_table;
表已更改。

-- 创建订单商品表
SQL> create table t_pdorder(
  pro_id char(12),
  pro_name varchar2(30),
  order_buy_num number(5),
  constraint fk_t_po foreign key(pro_id) references t_production(pro_id))
  tablespace order_table;

  表已创建。

```

### 添加数据
```sql
-- 管理员插入数据
SQL> insert into t_manager values(0101010101,12345678);

-- 商品表插入数据
SQL> insert into t_production(pro_id,pro_name,pro_num,pro_price,pro_restock) 
SQL> select dbms_random.string('x',12) pro_id,
  dbms_random.string('x',15) as pro_name,
  trunc(dbms_random.value(2,1000)) as pro_num,
  trunc(dbms_random.value(1,9999)) as pro_price,
  trunc(dbms_random.value(0,1)) as pro_restock
from dual
connect by level <= 100000;

已创建 100000 行。

-- 商品信息表插入数据
SQL> insert into t_pdorder( pro_id, pro_name)
SQL> select pro_id, pro_name from t_production;

已创建 100000 行。

SQL>update t_pdorder set order_buy_num =1 where rownum<=100000;

已更新 100000 行。
```
### 创建包
```sql
- 用户包
create or replace PACKAGE  user_package AS 
PROCEDURE dele_user(user_id in varchar2,  user_iden in char);
function get_order_total( p_order_id in char) return number;
END USER_PACKAGE;

- 用户包体
CREATE OR REPLACE
PACKAGE BODY USER_PACKAGE AS
  PROCEDURE dele_user(user_id in varchar2,  user_iden in char) AS
  BEGIN
        delete from t_user t where user_id = t.user_id;
        delete from t_order o where user_id = o.user_id;
  END dele_user;

  function get_order_total( p_order_id in char) return number AS
    o_total number; 
  BEGIN
  select order_pay into o_total from t_order where order_id = p_order_id;
  return o_total;
  END get_order_total;
  
END USER_PACKAGE;

- 管理员包
CREATE OR REPLACE 
PACKAGE SALE_PACKAGE AS 
PROCEDURE add_user(p_user_id in varchar2, p_user_account in char, p_user_age in number, p_user_sex in number, p_user_iden in char,
p_user_start in date, p_user_address in varchar2, p_user_pwd in char, p_user_tel in char ,p_user_register in number);
PROCEDURE dele_user(user_id in varchar2,  user_iden in char);
function get_order_total( p_order_id in char) return number;
END SALE_PACKAGE;

- 管理员包体
CREATE OR REPLACE
PACKAGE BODY SALE_PACKAGE AS

  PROCEDURE add_user(p_user_id in varchar2, p_user_account in char, p_user_age in number, p_user_sex in number, p_user_iden in char,
p_user_start in date, p_user_address in varchar2, p_user_pwd in char, p_user_tel in char ,p_user_register in number) AS
  BEGIN
    insert into t_user values(p_user_id, p_user_account, p_user_age, p_user_sex, p_user_iden , p_user_start, p_user_address, 
    p_user_pwd, p_user_tel, p_user_register);
  END add_user;
  
  PROCEDURE dele_user(user_id in varchar2,  user_iden in char) AS
  BEGIN
    delete from t_user t where user_id = t.user_id;
    delete from t_order o where user_id = o.user_id;
  END dele_user;
  
  function get_order_total( p_order_id in char) return number AS
    l_total number;
--    l_pro_price number := 0;
--    l_pro_num number :=0;
  BEGIN
  select order_pay into l_total from t_order where order_id = p_order_id;
  return l_total;
  END get_order_total;

END SALE_PACKAGE;

```

### 备份
- Oracle数据库备份方案的设计需要考虑以下几个方面：
- 1.备份类型：全量备份、增量备份、差异备份。
- 2.备份频率：每天备份、每周备份、每月备份。
- 3.备份存储：本地存储、远程存储、云存储。
- 4.备份恢复：恢复时间、数据完整性。
- 基于这些方面，下面是一个可能的Oracle数据库备份方案：
- 1.全量备份：每周执行一次全量备份，备份所有数据文件和控制文件。
- 2.增量备份：每天执行增量备份，备份在上次全量备份之后更改的数据块。
- 3.备份频率：每天备份一次，每周备份一次。
- 4.备份存储：将备份数据存储在本地磁盘和远程磁盘。本地磁盘用于快速备份和恢复，远程磁盘用于灾难恢复。
- 5.备份恢复：备份恢复时间应尽可能短，数据完整性必须得到保证。备份恢复测试应该定期进行，以确保备份文件的可用性和恢复时间。
- 6.备份监控：对备份过程进行监控，及时发现备份异常情况，保证备份的可靠性。
- 总之，Oracle数据库备份方案的设计需要综合考虑备份类型、备份频率、备份存储和备份恢复等多个方面，以确保备份数据的完整性和可用性。



# 实验2：用户及权限管理
# 学号：202010414102        姓名：曾玉霞

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
-- 创建角色conn_res_role并包含resource，connect角色和拥有创建视图的功能
create role conn_res_role;
grant connect, resource, create view to conn_res_role;

-- 创建用户sale，并分配表空间
create user sale identified by 123;
create tablespace SALETB datafile '/tmp/sale.dbf' size 50m;
alter user sale default tablespace SALETB;
grant conn_res_role to sale;
--收回角色
revoke conn_res_role from sale;
```

> sale是使用create user先创建好用户，再用create tablespace SALETB datafile '/tmp/sale.dbf' size 50m;创建表空间，再将用户的表空间修改成创建的SALETB表空间

- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
-- 赋予sale create session
grant create session to sale;
$ sqlplus sale/123@pdborcl
SQL> show user;
USER 为 "SALE"
SQL>select * from session_privs;
PRIVILEGE
----------------------------------------
CREATE SESSION

SQL>select * from session_roles;
未选定行

<!-- system/123@pdborcl -->
-- 解决sale权限不足
SQL> grant all privileges to sale;
SQL>create table customers (id number,name varchar(50)) tablespace SALETB;

SQL> insert into customers(id,name)values(1,'zhang');
insert into customers(id,name)values(2,'wang');

SQL> create view customers_view as select name from customers;

SQL> grant select on customers_view to hr;

SQL> select * from customers_view;
NAME
--------------------------------------------------
zhang
wang


```

- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> select * from sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> select * from sale.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
> sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL> alter profile default limit failed_login_attempts 3;    
配置文件已更改
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
用户已更改。
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='SALETB';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
SALETB
/tmp/sale.dbf
	50	    0 NO

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
 表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      370      25.75	 344.25      93.04
UNDOTBS1			      370    44.4375   325.5625      87.99
USERS				      370	   4	    366      98.92
SALETB				      370    48.9375   321.0625      86.77
SYSTEM				      370	   8	    362      97.84
SYSAUX				      100      25.75	  74.25      74.25
UNDOTBS1			      100    44.4375	55.5625      55.56
USERS				      100	   4	     96 	96
SALETB				      100    48.9375	51.0625      51.06
SYSTEM				      100	   8	     92 	92
SYSAUX					5      25.75	 -20.75       -415

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
UNDOTBS1				5    44.4375   -39.4375    -788.75
USERS					5	   4	      1 	20
SALETB					5    48.9375   -43.9375    -878.75
SYSTEM					5	   8	     -3        -60
SYSAUX				       50      25.75	  24.25       48.5
UNDOTBS1			       50    44.4375	 5.5625      11.13
USERS				       50	   4	     46 	92
SALETB				       50    48.9375	 1.0625       2.13
SYSTEM				       50	   8	     42 	84
SYSAUX				      260      25.75	 234.25       90.1
UNDOTBS1			      260    44.4375   215.5625      82.91

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
USERS				      260	   4	    256      98.46
SALETB				      260    48.9375   211.0625      81.18
SYSTEM				      260	   8	    252      96.92

已选择 25 行。
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL> drop role conn_res_role;
角色已删除。

SQL> drop user sale cascade;
用户已删除。
```

## 实验参考

- SQL-DEVELOPER修改用户的操作界面：
![](../img/sqldevelop修改用户.png)

- sqldeveloper授权对象的操作界面：
![](../img/sqldevelop授权对象.png)

## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。

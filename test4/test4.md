# 实验4：PL/SQL语言打印杨辉三角

- 学号：202010414102 姓名：曾玉霞

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

### 打印杨辉三角
```sql
create or replace procedure YHTriange(N in integer) 
as
    type t_number is varray (100) of integer not null;
    i integer;
    j integer;
    spaces varchar2(30) :='   '; 
    rowArray t_number := t_number();
begin
   dbms_output.put_line('1');
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line('');
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N
    loop
        rowArray(i):=1;    
        j:=i-1;
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line('');
    end loop;
end YHTriange;
/

输出 -- PL/SQL 过程已成功完成。

```

### 调用存储过程：

```sql

set serveroutput on;
declare
   begin
      YHTriange(10); 
   end;
/
```
### 输出
![image](https://gitlab.com/Zzyx333/oracle/-/raw/main/test4/pict2.png)



